console.log("Start of the Script");

window.onload = function()
{
    const canvas = document.getElementById("the-canvas");
    const cxt = canvas.getContext("2d");

    let marioSprite = document.getElementById("marioSprite");
    let blocks = document.getElementById("blocksSprite")
    let shopTheme = document.getElementById("music");
    let percentage = 0;
    
    const marioSpriteSize = 16;
    const spritsheetSize = 64;
    const scale = 3;
    const speed = 5;

    let frames = 4;
    let currentFrame = 0;
    let initial = new Date().getTime();
    let current;
    let currentDirection = 0;

    let playerXPos = 0 //default x cord
    let playerYPos = canvas.height * 0.75 //deafault y cord

   
   function GameObject(spritesheet, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight) 
   {
        this.spritesheet = spritesheet;
        this.sx = sx;
        this.sy = sy;
        this.sWidth = sWidth;
        this.sHeight = sHeight;
        this.dx = dx;
        this.dy = dy;
        this.dHeight = dHeight;
        this.dWidth = dWidth;
    }

    let block1 = new GameObject(blocks, 0, 0, 16, 16, 0, canvas.height /2, 16 * scale, 16 * scale );
    let block2 = new GameObject(blocks, 64, 0, 16, 16, 275 - (8 * scale), canvas.height /2, 16 * scale, 16 * scale );
    let block3 = new GameObject(blocks, 32, 0, 16, 16, 550 - (8 * scale), canvas.height /2, 16 * scale, 16 * scale );
    let block4 = new GameObject(blocks, 48, 0, 16, 16, 825 - (8 * scale), canvas.height /2, 16 * scale, 16 * scale );
    let block5 = new GameObject(blocks, 16, 0, 16, 16, canvas.width - (16 * scale), canvas.height /2, 16 * scale, 16 * scale );


    function startPos()
    {
        cxt.drawImage(marioSprite, (spritsheetSize / 4) * currentFrame, 0, 16, 16, playerXPos, playerYPos, (marioSpriteSize * scale), (marioSpriteSize * scale) )
    }
    startPos();



    function draw()
    {
        cxt.clearRect(0, 0, canvas.width, canvas.height);
        cxt.font = "60px Arial";
        cxt.fillStyle = "#00ccff";
        cxt.textAlign = "center";
        cxt.fillText("Wii Shop Channel", canvas.width / 2, 100);
       // console.log("draw function working before blocks");
        cxt.save();

        cxt.drawImage(block1.spritesheet, block1.sx, block1.sy, 
            block1.sWidth, block1.sHeight, 
            block1.dx, block1.dy, block1.dWidth, 
            block1.dHeight);
        cxt.drawImage(block2.spritesheet, block2.sx, block2.sy, 
                    block2.sWidth, block2.sHeight, 
                    block2.dx, block2.dy, block2.dWidth, 
                    block2.dHeight);
        cxt.drawImage(block3.spritesheet, block3.sx, block3.sy, 
                    block3.sWidth, block3.sHeight, 
                    block3.dx, block3.dy, block3.dWidth, 
                    block3.dHeight);
        cxt.drawImage(block4.spritesheet, block4.sx, block4.sy, 
                    block4.sWidth, block4.sHeight, 
                    block4.dx, block4.dy, block4.dWidth, 
                    block4.dHeight);
        cxt.drawImage(block5.spritesheet, block5.sx, block5.sy, 
                    block5.sWidth, block5.sHeight, 
                    block5.dx, block5.dy, block5.dWidth, 
                    block5.dHeight);

       cxt.restore();

       //console.log("draw function after blocks");
  
    }

    function animateRight() //using the simple version of the code to start
    {
        cxt.clearRect(0, 0, canvas.width, canvas.height);
        draw();
        current = new Date().getTime(); //setting current time to right now

        if (current - initial >= 150)
        {
            currentFrame = (currentFrame + 1) % frames;
            console.log("current frame is " + currentFrame);
            initial = current;
        }

        cxt.drawImage(marioSprite, (spritsheetSize / 4) * currentFrame, 0, 16, 16, playerXPos, playerYPos, (marioSpriteSize * scale), (marioSpriteSize * scale) )

        currentDirection = 0;
        
    }

    function animateLeft() //using the simple version of the code to start
    {
        cxt.clearRect(0, 0, canvas.width, canvas.height);
        draw();
        current = new Date().getTime(); //setting current time to right now

        if (current - initial >= 150)
        {
            currentFrame = (currentFrame + 1) % frames;
            console.log("current frame is " + currentFrame);
            initial = current;
        }

        cxt.drawImage(marioSprite, (spritsheetSize / 4) * currentFrame, 16, 16, 16, playerXPos, playerYPos, (marioSpriteSize * scale), (marioSpriteSize * scale) )
        currentDirection = 1;
        
    }


    function GamerInput(input) {
        this.action = input; // Hold the current input as a string
    }
    
    let gamerInput = new GamerInput("None");

    function input(event) {
    
        if (event.type === "keydown") {
            switch (event.keyCode) {
                case 13:
                    gamerInput = new GamerInput("Enter")
                    break;
                case 68: // D
                    gamerInput = new GamerInput("D")
                    break;
                case 65: // A
                    gamerInput = new GamerInput("A")
                    break;
                default:
                    gamerInput = new GamerInput("None"); //No Input
            }
        } else {
            gamerInput = new GamerInput("None");
        }

    }

    function idleSprite()
    {
        switch(currentDirection) //idle sprite
            {
                case 0:
                    cxt.clearRect(0, 0, canvas.width, canvas.height);
                    draw();
                    cxt.drawImage(marioSprite, 0, 0, 16, 16, playerXPos, playerYPos, (marioSpriteSize * scale), (marioSpriteSize * scale) );
                    break;
                case 1:
                    draw();
                    cxt.clearRect(0, 0, canvas.width, canvas.height);
                    draw();
                    cxt.drawImage(marioSprite, 0, 16, 16, 16, playerXPos, playerYPos, (marioSpriteSize * scale), (marioSpriteSize * scale) );  
                    break;
                default:
            }
    }

    function collisionDetection()
    {
        if (playerXPos <= 0)
        {
            playerXPos = canvas.width - ((marioSpriteSize * scale) + 1);
            cxt.drawImage(marioSprite, 0, 16, 16, 16, playerXPos, playerYPos, (marioSpriteSize * scale), (marioSpriteSize * scale) );
        }
        if (playerXPos > canvas.width)
        {
            playerXPos = 0;
            cxt.drawImage(marioSprite, 0, 0, 16, 16, playerXPos, playerYPos, (marioSpriteSize * scale), (marioSpriteSize * scale) );
        }
    }

    function percentageDL()
    {
        setInterval(percentChange, 5000);
        function percentChange()
        {
            percentage += 1;
            document.getElementById("percentage").innerHTML = percentage + "%";
        }

        if (percentage >= 99)
        {
            //clearInterval(percentChange)
            document.getElementById("percentage").innerHTML = "ERROR";
        }
    }

    function update() {

        //percentageDL();

        if (gamerInput.action === "D")
        {
            playerXPos += speed;
            animateRight();
            collisionDetection();
        }
        if (gamerInput.action === "A")
        {
            playerXPos -= speed;
            animateLeft();
            collisionDetection();

        } 
        if (gamerInput.action === "Enter")
        {
            idleSprite();
            shopTheme.play();

        }
        if (gamerInput.action === "None")
        {
            idleSprite();
         
        }
    }

    percentageDL(); //calling it here becuase it refreshed too fast in the update function


    function gameloop()
    {
        draw();
        update();
        window.requestAnimationFrame(gameloop);
    }

    window.requestAnimationFrame(gameloop);
    window.addEventListener('keydown', input);
    window.addEventListener('keyup', input);



   
}

